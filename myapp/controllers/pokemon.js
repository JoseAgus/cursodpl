
const Pokemon = require("../models/Pokemon");

let Bicicleta = require("../models/Pokemon");

exports.pokemon_list = function (req, res) {
    res.render("pokemons/index", { pokes: Pokemon.allPokemons });

}


exports.pokemon_create_get = function (req, res) {

    res.render("pokemons/create");

}//metodo get create


exports.pokemon_create_post = function (req, res) {

    let poke = new Pokemon(req.body.id, req.body.nombre, req.body.ataque, req.body.defensa);

    Pokemon.add(poke);

    res.redirect("/pokemons"); //Redirecciono al index

}//metodo post create



exports.pokemon_delete_post = function (req, res) {

    Pokemon.borrarById(req.body.id);

    res.redirect("/pokemons");

};//metodo post borrar


exports.pokemon_update_get = function(req,res) {

    let poke = Pokemon.findById(req.params.id); //Coje el pokemon por el Id

    res.render("pokemons/update", {poke});

}//metodo get update

exports.pokemon_update_post = function(req,res) {

    let poke = Pokemon.findById(req.params.id);

    poke.id = req.body.id;

    poke.nombre =  req.body.nombre;

    poke.ataque = req.body.ataque;

    poke.defensa = req.body.defensa;

    // Pokemon.findById(poke);

    res.redirect("/pokemons");

}//metodo post update
