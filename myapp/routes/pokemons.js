

var express = require('express');

var router = express.Router();



let pokemonController = require('../controllers/pokemon');



router.get("/", pokemonController.pokemon_list);

//Rutas Create
router.get("/create", pokemonController.pokemon_create_get);
router.post("/create", pokemonController.pokemon_create_post);

//Ruta Borrar
router.post("/:id/delete", pokemonController.pokemon_delete_post);

//Rutas Update
router.get("/:id/update", pokemonController.pokemon_update_get);
router.post("/:id/update", pokemonController.pokemon_update_post);

module.exports = router;


