
/// Constructor de los pokemons 
let Pokemon = function (id, nombre, ataque, defensa) {
    this.id = id;
    this.nombre = nombre;
    this.ataque = ataque;
    this.defensa = defensa;
}


Pokemon.allPokemons = [];


Pokemon.add = function (poke) {

    this.allPokemons.push(poke);

}//Metedo de añadir pokemons



Pokemon.borrarById = function (pokeId) {

    for (let i = 0; i < Pokemon.allPokemons.length; i++) {

        if (Pokemon.allPokemons[i].id == pokeId) {

            Pokemon.allPokemons.splice(i, 1);

            break;

        }

    }

}//Metodo para Borrar pokemons por id


Pokemon.findById = function(pokeId) {

    let poke = Pokemon.allPokemons.find(x => x.id == pokeId);

    if (poke)

        return poke;

    else

        throw new Error(`No existe un Pokemon con el id ${pokeId}`);

}//Metodo para editar el pokemon


let a = new Pokemon(1, "Pikachu", 31, 34);

let b = new Pokemon(2, "Tyranitar", 100, 180);

Pokemon.add(a);
Pokemon.add(b);


module.exports = Pokemon;